<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Habitacion;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Reservas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fechaEntrada')->textInput() ?>

    <?= $form->field($model, 'fechaSalida')->textInput() ?>

    
    <?php
    $habi = Habitacion::find()->all();
    $listado = ArrayHelper::map($habi,'numhabit','idtipo');

    echo $form->field($model,'numhabit')->dropDownList(
            $listado, ['prompt' => 'Selecciona la habitacion a reservar']
    );
 
    
    ?>

   

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iva')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
