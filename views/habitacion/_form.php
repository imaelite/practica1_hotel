<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Tipo;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Habitacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="habitacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $tipos=Tipo::find()->all();
    $listado=ArrayHelper::map($tipos,'idtipo','categoria');

    echo $form->field($model, 'idtipo')->dropDownList(
            $listado,
            ['prompt'=>'Selecciona el tipo de habitacion']
            );
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
