<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Habitacion */

$this->title = 'Actualizar Habitacion: ' . $model->numhabit;
$this->params['breadcrumbs'][] = ['label' => 'Habitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numhabit, 'url' => ['view', 'id' => $model->numhabit]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="habitacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
