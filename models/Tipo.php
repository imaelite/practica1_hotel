<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo".
 *
 * @property int $idtipo
 * @property string $categoria
 * @property string $desripcion
 * @property double $precioHabitacion
 *
 * @property Habitacion[] $habitacions
 */
class Tipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precioHabitacion'], 'number'],
            [['categoria', 'desripcion'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtipo' => 'Idtipo',
            'categoria' => 'Categoria',
            'desripcion' => 'Desripcion',
            'precioHabitacion' => 'Precio Habitacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabitacions()
    {
        return $this->hasMany(Habitacion::className(), ['idtipo' => 'idtipo']);
    }
}
